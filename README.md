# agdg-game

/agdg/ makes a game

# Guidelines
Everyone contributing must use a own branch for the intended change.
Make sure your master is fresh (otherwise do a git-pull), then create a new branch.
Once you're done with your changes, commit, push and create a merge-request.
I will review and then merge. 

Working with Godot and it's source as a Git-Repository, we have to be aware that changes are not reflected in the Editor automatically.
Whenever you plan to pull in changes from git (applicable when you want to update master):
1. Scene -> Close Scene
4. Git-Pull
5. Reopen-Scene (Scene -> Open Recent)

Be aware that if you've made local changes on master (which you shouldn't have because you should made changes in your own branch), those changes are lost.

# GDD
https://gitlab.com/goldanon/agdg-game/blob/master/GDD.md

# Web Build
you can try the latest build here, without running a scary .exe locally\
https://goldanon.gitlab.io/agdg-game

# Contributors
- goldanon
- DONODO
- waffle99
- Mecha
- put your name here

extends Node
var player
var animator
var speed = 0
var accelerate = 0.01
var brake = 0.1

func _ready():
  player = get_node("Ship")
  animator = get_node("AnimationPlayer")
	
func _process(delta):
	if speed < 0:
		$Ship.flip_h = true
		$Ship/Exhaust.flip_h = true
		$Ship/Exhaust.position.x = $Ship.region_rect.position.x + 50
	else:
		$Ship.flip_h = false
		$Ship/Exhaust.flip_h = false
		$Ship/Exhaust.position.x = $Ship.region_rect.position.x - 50
		
	if Input.is_action_pressed("ui_left"):		
		if speed < 0:
			speed -= accelerate
		else:	
			speed -= brake		
		animator.play("exhaust")
	if Input.is_action_pressed("ui_right"):		
		if speed > 0:
			speed += accelerate
		else:
			speed += brake	
		animator.play("exhaust")
	if Input.is_action_pressed("ui_up"):		
		player.position.y -= 1
		animator.play("exhaust")
	if Input.is_action_pressed("ui_down"):		
		player.position.y += 1
		animator.play("exhaust")
	if Input.is_action_just_released("ui_right") || Input.is_action_just_released("ui_left"):
		animator.stop(true)
	
	
	player.position.x += speed
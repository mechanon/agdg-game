extends ParallaxBackground

var p1
var p2
var p3

export(Vector2) var size #texture size
export(Array,Texture) var textures #
export(Array,Vector2) var region #texture regions
export(Array,Vector2) var speeds #parallax speeds

var parallaxLayers = []
var bl = true
var player

func _ready():
	
	player = $"/root/Node2D/Player"
	
	#Create parallax layers
	for i in range(0,textures.size()):
		
		var node = ParallaxLayer.new()
		var sprite = Sprite.new()

		node.set_name(str(i))
		node.set_mirroring(Vector2(size.x,1))
		
		sprite.set_name(str(i))
		sprite.texture = textures[i]
		sprite.region_enabled = true
		sprite.centered = false
		sprite.region_rect = Rect2(region[i],size)

		node.add_child(sprite)
		
		parallaxLayers.append(node)
		
		self.add_child(node)
		
	pass
	
func _physics_process(delta):

	#move parallax
	for i in range(0,speeds.size()):
		parallaxLayers[i].motion_offset += speeds[i] * player.speed
	
	pass

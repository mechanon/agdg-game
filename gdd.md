# Game Design Document

/agdg/ Community Game

## Overview

This game is a Shmup playing in a post-apocalyptic human like-planet setting after Machines have taken control over the planet.

Player starts by flying a ship out of his base into a Jungle where he encounters first smaller enemies, 
collecting loot such as energy balls and crystals.\
He can change into higher altitudes for stronger enemies.

During the game he can upgrade his space ship with better engines, once he did reach a certain engine upgrade, he can fly into space where the challenge gets harder.\
Using key Items he received from higher altitudes, he can enter underground levels.

# Target Platform
Windows, Linux, Web(if possible)

# Visual Style
flat shaded Pixel-graphics, style similar to NES, C64, ATARI games 

![alt text](https://gitlab.com/goldanon/agdg-game/uploads/370717b9067363b3a9d9b39c89c94b98/somethinglikethis.png)
![alt text](https://gitlab.com/goldanon/agdg-game/uploads/a8af27bcbf9e9c632626f2fc6b9a2444/enemy2.png)

# Audio Style
chiptune

# Gameplay
Player starts flying his ship into the right direction, shooting smaller enemies.
With collected resources he can buy upgrades.
During the game he encounters harder enemies and mid-bosses which he can only beat if he has certain upgrades

## Intro
Fade in Name of game (tbd), blinking "press start"\
optional demo: static pictures with text explaining the setting

## Starting out
On start, load Hangar view showing ship and current weapon, upgrades.\
Button "launch" starts action view.\
Flying out of base, player can get accustom to controls (accelerate, slow down), discover flying left is possible. \
Reach Jungle area with first enemies, collecting loot. Discover changing altitude is possible\
Reach first mid-boss, high difficulty if player is too low on upgrades.

# Upgrades
- Engine
- Shield

# Weapons
**Projectiles**
- Bullets
- Rockets
- Lasers

**Effects**
- double, tripple Fire
- swirl
- charged, high damage, needs to charge for a certain time

**Items**
- EMP Bomb, paralyze enemies
# Enemies
**Jungle hideout**
* Fewest enemies
* low-altitude ships, geometric, saucer-shaped

**Over the ocean / In the clouds**

Low  
* Enemy boats in the background fire at you

High 
* Enemies will typically not shoot if they cannot see you

**Desert / Desert Sky**

Low 
* tbd

High 
* Lots of flak fire and massed enemy fighters makes it dangerous.

**Edge of space**
* Larger enemies that shoot many bullets - more bullet hell than the other stages
* big motherships

**Enemy fortress (outside)**
* Lots of static defenses and trap-type obstacles
* Mechanical enemies

**Enemy fortress (inside)**
* TBD

**Caves**
* TBD

**Underground laboratory**
* TBD

# Levels
3 world types, accessible by changing Altitudes:
 - ground 
 - atmosphere
   -> needs engine upgrade +2
 - underground
   -> needs key 

Certain levels are accesible trough keys, found in certain areas (i.e. key in space received after boss-fight opens gate to deeper underground)

![alt text](https://gitlab.com/goldanon/agdg-game/uploads/54f6d7cfbd2725d9833e4d893f6af416/stages1.png)
**Jungle hideout**
* Fewest enemies

**Over the ocean / In the clouds**
* Can switch altitudes at any time.

Low ->

* High speed - higher speed low near ocean surface due to ground effect reducing drag.

High ->

* Vision is obscured by clouds - you have a limited sight radius when inside
* The lane to go to the edge of space is found only in the high part. If it is not taken, you will go to the desert.

**Desert / Desert Sky**

Low ->
* Sand dunes as obstacles

High ->
* TBD

**Edge of space**
* Moons, Planets, large Space-stations seen in background

**Enemy fortress (outside)**
* Lots of static defenses and trap-type obstacles

**Enemy fortress (inside)**
* Obstacles
* intense
* relatively short

**Caves**
* Underneath the fortess
* Multiple paths through the caves

**Underground laboratory**
* Final area, hardest
 
## Level Selection
Overmap in hangar view, button "map"


# TODO: Controls
Describe how the player controls his character, fires his weapon, etc etc.

## TODO: Modes
What modes are available to the player, if any?


## TODO: In-Game HUD & Menus
TBD

# TODO: Main Menu
TBD

## TODO: Player Customization / Store
List the elements and options for the UI involved with setting up the player, buying and selling weapons, equiping items, etc etc.  
Flow Chart and/or Wireframe

## TODO: Game setup Screen 
Flow Chart and/or Wireframe

## TODO: Game Over Screen
What information is displayed once a match is finished?

# TODO: Walktrough
Go over the gameplay from start to finished.. Break this section up into the setup in the beginning, what the first couple things are that happens when the match starts. How do the players prepare or get ready?

What's the middle game like, what will the player spend the majority of his time doing while playing the game?
What are our emotional objectives in the player, what kind of gameplay are we trying to foster here and how are we achieving or working towards those goals?
What kind of play styles are available to the player?  
How does the player interact with the environment?  
What are the main challenges facing the player and how are we rewarding the player for successful behavior.
Describe the multiplayer aspect of gamplay both from the adversarial/competitive aspect and from the co-operative aspect
Mechanics
What kind of mechanics are to be employed in the game?  Go over movement, combat, health, equipment usage, special abilities and anything else that governs how the player plays the game.

# TODO: WINNING 
Describe the conditions for winning the game.  How long do matches/games last?  What determines when the game is over?

## TODO: Score
After the match is over, how is the score calculated and how are

## TODO: Currency
How much money do the players earn per match or per action?  Are there modifiers applied to this amount for specific actions?

## TODO: Challenges of importance
Are there certain challenges, events or levels within the game that are more important and challenging than others.  Challenges that represent large progress jumps and can unlock new items, abilities or areas

## TODO: Missions and Achievements
Is there a mission or achievement system? How does it work?  What is the purpose of this system? Go over all achievements and rewards

# TODO: Assets
A technical documentation describing the requirements of all assets created for the game.

2D: Style and Requirements for model sheets, environmental illustrations, and promo pieces.

Textures: Dimensions, maps needed and file formats

Naming conventions with examples

Animation requirements: Rigging limits, Programs to be used for rigging and animation


A full asset list, budget and schedule will be laid out within a spreadsheetw

## TODO: Characters
A run down of all of the 2D, 3D, Animation and Audio work needed for the characters of the game.  Including back story, abilities and special traits

## TODO: Weapons
A run down of all of the 2D, 3D, VFX and Audio work needed for the weapons of the game

All stats and abilities of each weapon to be described in detail as well as their cost

## TODO: Equipment and Upgrades
A run down of all of the 2D, 3D and Audio work needed for the equipment of the game

All costs, abilities and bonuses associated with equipment to be laid out in detail

## TODO: Environmental
All global environmental assets(which could be reskinned depending on the level they appear in) to be laid out here as well as their role in the puzzle process and their possible effect on the scene and environment around them.

## TODO: Audio
All global audio assets like UI sounds and audio tracks

# Back Story
Set 15 years after humanity lost the war against a mysterious machine foe, humanity lives life on the run from mechanical hunters. You are the pilot of a low-altitude ship you and your friends have cobbled-together from the hulks of fallen enemies. The hope of your tribe, your mission is to find and assault enemy outposts and fortresses (expansive levels with lots of static defenses and many enemies) to find a way to reverse humanity's defeat, but what you begin to find is that the machines have not been idle. They have been searching for a way to end all life in the universe.
